#include <gtk/gtk.h>
#include <gio/gdesktopappinfo.h>

#define SIZE          10
#define SCHEMA_ID     "sm.puri.phosh"
#define FAVORITES_KEY "favorites"

GtkWidget      *fbox;
GtkButton     **buttons;
guint          *indices;
guint           button_list_length;
GSettings      *settings;
GHashTable     *hash_table;
unsigned long   favorites_changed_handler_id;

static void on_favorites_changed (GObject *, gpointer); // fwd decl

static void
display_list (GStrv favs)
{
  printf ("Fav list is as: \n");
  for (int i = 0; favs[i]; ++i)
    printf ("%s, ", favs[i]);
  printf ("\n\n");
}


/**
 * triggered when elements are rearranged,
 * NOT when elements are added/removed
*/
static void
on_drop_target (GtkDropTarget *target,
                GValue        *value,
                GtkButton     *drop_button)
{
  GtkButton *drag_button = g_value_get_object (value);
  g_print ("Drag button: %s, drop button: %s\n",
           gtk_button_get_label (drag_button),
           gtk_button_get_label (drop_button));

  if (drag_button == drop_button)
    return;

  g_auto (GStrv) fav_list = g_settings_get_strv (settings, FAVORITES_KEY);
  gchar *drag_label = g_strdup (gtk_button_get_label (drag_button));
  gchar *drop_label = g_strdup (gtk_button_get_label (drop_button));
  guint *drag_app_hv = g_hash_table_lookup (hash_table, drag_button);
  guint *drop_app_hv = g_hash_table_lookup (hash_table, drop_button);
  gint i, start, end, shift;

  start = *drag_app_hv, end = *drop_app_hv;
  shift = start < end ? 1 : -1;
  
  printf ("DND operation drag (%d) -> drop (%d)\n", start, end);

  for (int i = start; i != end; i += shift) {
    gchar *label_next = g_strdup (gtk_button_get_label (buttons[i + shift]));
    gtk_button_set_label (buttons[i], label_next);
    fav_list[i] = g_strdup (label_next);
    printf ("Transition: button %d -> %d\n", i + shift, i);
  }
  gtk_button_set_label (drop_button, drag_label);
  fav_list[end] = drag_label;

  display_list (fav_list);

  g_signal_handler_disconnect (settings, favorites_changed_handler_id);
  g_settings_set_strv (settings, FAVORITES_KEY, fav_list);
  favorites_changed_handler_id = g_signal_connect (settings,
                                                   "changed::" FAVORITES_KEY,
                                                   G_CALLBACK (on_favorites_changed),
                                                   NULL);
}



static void
add_drop_target (GtkButton *button)
{
  GType types[1] = { GTK_TYPE_BUTTON };
  GtkDropTarget *target = gtk_drop_target_new (G_TYPE_INVALID, GDK_ACTION_COPY);
  gtk_drop_target_set_gtypes (target, types, 1);
  g_signal_connect (target, "drop", G_CALLBACK (on_drop_target), button);
  gtk_widget_add_controller (GTK_WIDGET (button), GTK_EVENT_CONTROLLER (target));
}


static GdkContentProvider *
on_drag_prepare (GtkDragSource *source,
                 GtkButton     *button)
{
  GdkContentProvider *content[1] = {
    gdk_content_provider_new_typed (GTK_TYPE_BUTTON, button)
  };

  return gdk_content_provider_new_union (content, 1);
}


static void
on_drag_begin (GtkDragSource *source,
               GdkDrag       *drag,
               GtkButton     *button)
{
  GdkPaintable *paintable = gtk_widget_paintable_new (GTK_WIDGET (button));

  gtk_drag_source_set_icon (source, paintable, 0, 0);
  g_object_unref (paintable);
}


static void
add_drag_source (GtkButton *button)
{
  GtkDragSource *source = gtk_drag_source_new ();
  g_signal_connect (source, "prepare", G_CALLBACK (on_drag_prepare), button);
  g_signal_connect (source, "drag-begin", G_CALLBACK (on_drag_begin), button);
  gtk_widget_add_controller (GTK_WIDGET (button), GTK_EVENT_CONTROLLER (source));
}


static void
clear_app_list ()
{
  if (!buttons)
    return;

  int i = 0;

  while (buttons[i]) { // buttons[] is NULL-terminated
    size_t *idx = g_hash_table_lookup (hash_table, buttons[i]);
    printf ("Removing button #%d...\n", *idx);
    GtkButton *b = buttons[i];

    if (g_object_is_floating (b))
      g_object_ref_sink (b); // take ownership if floating

    if (gtk_widget_get_parent (GTK_WIDGET (buttons[i]))) {
      // gtk_widget_unparent (gtk_widget_get_parent (GTK_WIDGET (buttons[i])));
      gtk_flow_box_remove (fbox, GTK_WIDGET (buttons[i]));
    }
    else {
      g_object_unref (b);
    }

    ++i;
    if (!GTK_IS_BUTTON (b))
      printf ("Removed!\n");
  }
  g_free (buttons);
  buttons = NULL;
  g_free (indices);
  indices = NULL;
}


static void
on_favorites_changed (GObject  *emitter,
                      gpointer  user_data)
{
  g_print ("%s: Favorites changed (or init call), recreating apps list!\n", __func__);
  g_auto (GStrv) fav_list = g_settings_get_strv (settings, FAVORITES_KEY);
  guint len = g_strv_length (fav_list);

  /* remove old list */
  clear_app_list ();

  /* extre element for NULL */
  buttons = g_new (GtkButton *, len + 1);
  indices = g_new (guint, len + 1);

  for (int i = 0; fav_list[i]; ++i) {
    gchar *label = g_strdup (fav_list[i]);

    GtkButton *btn = gtk_button_new_with_label (label);
    buttons[i] = btn;
    indices[i] = i;
    g_hash_table_insert (hash_table, buttons[i], &indices[i]);

    if (g_desktop_app_info_new (fav_list[i])) {
      add_drag_source (buttons[i]);
      add_drop_target (buttons[i]);
      gtk_widget_set_size_request (GTK_BUTTON (buttons[i]), 25, 25);

      GtkFlowBoxChild *child = gtk_flow_box_child_new ();
      gtk_flow_box_child_set_child (child, GTK_WIDGET (buttons[i]));
      gtk_flow_box_append (fbox, GTK_WIDGET (child));
    }
  }
  buttons[len] = NULL;
}


static void
setup_layout (GtkFlowBox *fbox)
{
  favorites_changed_handler_id = g_signal_connect (settings,
                                                   "changed::" FAVORITES_KEY,
                                                   G_CALLBACK (on_favorites_changed),
                                                   NULL);
  /**
   * everytime a list is changed from Home (app add/remove)
   * this function is called, which just creates a new apps list
   * based on the updated favorites key and discards the old one
  */
  on_favorites_changed (settings, NULL);

  for (int i = 0; buttons[i]; ++i) {
    guint *idx = g_hash_table_lookup (hash_table, buttons[i]);
    printf ("Button: #%d, label: %s\n", *idx, gtk_button_get_label (buttons[i]));
  }

  // clear_app_list ();
}


static void
on_activate (GtkApplication *app,
             gpointer        user_data)
{
  GtkWidget *window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "* Favorites flowbox *");
  gtk_window_set_default_size (GTK_WINDOW (window), 800, 20);

  fbox = gtk_flow_box_new ();

  settings = g_settings_new (SCHEMA_ID);
  hash_table = g_hash_table_new (&g_direct_hash, &g_direct_equal);

  setup_layout (fbox);

  gtk_window_set_child (GTK_WINDOW (window), fbox);
  gtk_window_present (GTK_WINDOW (window));
}


int main ()
{
  GtkApplication *app = gtk_application_new ("com.rudra.app", G_APPLICATION_DEFAULT_FLAGS);

  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);
  g_application_run (app, 0, NULL);
  g_object_unref (app);

  return EXIT_SUCCESS;
}